# Test task for MT Finans:
Необходимо написать одностраничное Web-приложение, интерфейс которого содержит:
1) Таблицу, отображающую информацию о времени создания и удаления виртуальных серверов
(Столбцы: VirtualServerId, CreateDateTime, RemoveDateTime, SelectedForRemove);
2) Кнопку добавления нового виртуального сервера (Add);
3) Кнопку удаления виртуальных серверов помеченых SelectedForRemove (Remove);
4) Поле, в котором выводится суммарное время, в течении которого в таблице присутсвовал
хотябы один неудаленный сервер (TotalUsageTime);
5) Поле, в котором выводится текущее время (CurrentDateTime).
### 
## Before running the app pls run in both directories: ./front & ./back
```
npm install
```

## Make sure that you use redis server on port: 6379, host: 127.0.0.1
-used for caching records (3/5 mins);

### to launch up front-end app from ./front:
```
npm run start
```

### to start server app from ./back:
```
npm run start
```

### MongoDB was used as a DataBase;
## Working with a sandbox from https://mlab.com;

#### Improvments which can be done:
-- implement react-router