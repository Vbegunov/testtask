const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const dotenv = require('dotenv');
const env =dotenv.config().parsed;

const envKeys = Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next]);
    return prev;
  }, {});

module.exports = env => {
	return {
	entry: './js/App.js',
	output: {
	  path: path.resolve(__dirname, 'dist'),
	  filename: '[name].bundle.js',
	  chunkFilename: '[name].bundle.js',
	  publicPath: '/dist/'
	},
	plugins: [
		new CleanWebpackPlugin(['dist']),
		new webpack.DefinePlugin(envKeys),
	],
	module: {
	  rules: [
		{
		  test: /\.js$/,
		  exclude: /node_modules/,
		  use: {
			loader: "babel-loader"
		  }
		},
		{
		  test: /\.scss$/,
		  use: [
			"style-loader", // creates style nodes from JS strings
			"css-loader", // translates CSS into CommonJS
			"sass-loader" // compiles Sass to CSS
		  ]
		},
		{
			test: /\.css$/,
			use: [ 'style-loader', 'css-loader' ]
		},
		{
			test: /\.(png|jpg|gif|ttf)$/,
			use: [
			  {
				loader: 'file-loader',
				options: {}
			  }
			]
		  }
	  ]
	},
	resolve: {
		alias: {
			components:   	path.resolve(__dirname, './js/components/'),
			interface:   	path.resolve(__dirname, './js/components/interface')	  
		}
	}
  }
}