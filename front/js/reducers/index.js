import servers from './servers';
import { combineReducers } from 'redux';
export default combineReducers({
	servers,
});