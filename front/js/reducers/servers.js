import {
	ON_FETCH_SERVERS,
	ON_FETCH_SERVERS_OK,
	ON_FETCH_SERVERS_ERROR,
	ON_DELETE_SERVERS,
	ON_DELETE_SERVERS_ERROR, 
	ON_DELETE_SERVERS_OK,
	ON_ADD_SERVER,
	ON_ADD_SERVER_ERROR,
	ON_ADD_SERVER_OK,
} from '../actions/servers';

const defaultState = {
	items: [],
};

const reducer = (state = defaultState, action) => {
	let newState = Object.assign({}, state);
	switch(action.type) {
		case ON_FETCH_SERVERS:
			return state;

		case ON_FETCH_SERVERS_ERROR:
			return state;

		case ON_FETCH_SERVERS_OK:
			newState.items = action.servers;
			return newState;

		case ON_DELETE_SERVERS:
			return state;

		case ON_DELETE_SERVERS_ERROR:
			return state;

		case ON_DELETE_SERVERS_OK:
			return newState;
		case ON_ADD_SERVER:
			return state;

		case ON_ADD_SERVER_ERROR:
			return state;

		case ON_ADD_SERVER_OK:
			return newState;
		default:
			return state;
	}
};

export default reducer;