import axios from 'axios';

class request {
	constructor() {
		if(!request.instance) {
			request.instance = this;
		}
		return request.instance;
	}

	get(cfg) {
		return new Promise((resolve, reject) => {
			axios.get(`${cfg.url}`, {
				params: cfg.data,
			}).then(response => {
				resolve(response.data);
			}).catch(err => {
				reject(err);
			});
		});
	}

	post(cfg) {
		return new Promise((resolve, reject) => {
			axios.post(`${cfg.url}`, cfg.data).then(response => {
				resolve(response.data);
			}).catch(err => {
				reject(err);
			});
		});
	}

	delete(cfg) {
		return new Promise((resolve, reject) => {
			axios.delete(`${cfg.url}`, {data: cfg.data}).then(response => {
				resolve(response.data);
			}).catch(err => {
				reject(err);
			});
		});
	}
}

let setCookie = (name, value, options) => {
	options = options || {};

	var expires = options.expires;

	if (typeof expires == "number" && expires) {
		expires = options.expires = new Date(Date.now() + expires);
	}

	value = encodeURIComponent(value);
	var updatedCookie = name + "=" + value;

	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}
	document.cookie = updatedCookie;
};

let getCookie = (name) => {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
};

const api = new request();
export {
	api,
	setCookie,
	getCookie,
};