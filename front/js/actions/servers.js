import * as helper from '../utils/helper';

export const ON_FETCH_SERVERS 			= 'ON_FETCH_SERVERS';
export const ON_FETCH_SERVERS_OK 		= 'ON_FETCH_SERVERS_OK';
export const ON_FETCH_SERVERS_ERROR 	= 'ON_FETCH_SERVERS_ERROR';
export const ON_DELETE_SERVERS 			= 'ON_DELETE_SERVERS'; 
export const ON_DELETE_SERVERS_ERROR 	= 'ON_DELETE_SERVERS_ERROR'; 
export const ON_DELETE_SERVERS_OK 		= 'ON_DELETE_SERVERS_OK';
export const ON_ADD_SERVER 				= 'ON_ADD_SERVER';
export const ON_ADD_SERVER_ERROR 		= 'ON_ADD_SERVER_ERROR';
export const ON_ADD_SERVER_OK 			= 'ON_ADD_SERVER_OK';

const API_SERVER = process.env.API_SERVER;

export const onAddServer = () => ({
	type: ON_ADD_SERVER,
});

export const onAddServerError = () => ({
	type: ON_ADD_SERVER_ERROR,
});

export const onAddServerOk = (payload) => ({
	type: ON_ADD_SERVER_OK,
	server: payload.server,
});

export const addServer = () => {
	return dispatch => {
		dispatch(onAddServer());

		helper.api.post({
			url: `${API_SERVER}/api/v1/servers` ,
		}).then(response => {
			onAddServerOk(response);
		}).catch(reject => {
			dispatch(onAddServerError());
		});
	}
}

export const onDeleteServers = () => ({
	type: ON_DELETE_SERVERS,
});

export const onDeleteServersError = () => ({
	type: ON_DELETE_SERVERS_ERROR,
});

export const onDeleteServersOk = (payload) => ({
	type: ON_DELETE_SERVERS_OK,
	deleted_ids: payload.ids,
});

export const deleteServers = (ids) => {
	return dispatch => {
		dispatch(onDeleteServers());

		helper.api.delete({
			url: `${API_SERVER}/api/v1/servers`,
			data: {
				ids: ids
			}
		}).then(response => {
			dispatch(onDeleteServersOk(response));
		}).catch(error => {
			dispatch(onDeleteServersError());
		});
	}
}

export const onFetchServers = () => ({
	type: ON_FETCH_SERVERS,
});

export const onFetchServersError = () => ({
	type: ON_FETCH_SERVERS_ERROR,
});

export const onFetchServersOk = (payload) => ({
	type: ON_FETCH_SERVERS_OK,
	servers: payload.servers,
});

export const fetchServers = () => {
	return (dispatch) => {

		dispatch(onFetchServers());

		helper.api.get({
			url: `${API_SERVER}/api/v1/servers`,
		}).then(response => {
			dispatch(onFetchServersOk(response));
		}).catch(error => {
			dispatch(onFetchServersError());
		});
	};
};