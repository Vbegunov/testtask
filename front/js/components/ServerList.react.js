import React, {Component} 	from 'react';
import Server 				from './Server.react';
import Button 				from '@material-ui/core/Button';
import moment 				from 'moment';
// styles
import './ServerList.scss'

export default class ServerList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			serversToDelete: [],
		};
	}

	setForDelete = (id) => {
		this.setState(state => {
			return {
				serversToDelete: [...state.serversToDelete, ...[id]]
			}
		});
	}

	removeFromDelete = (removeId) => {
		this.setState(state => {
			return {
				serversToDelete: state.serversToDelete.filter(id => id !== removeId),
			}
		});
	}

	deleteServers = () => {
		const { removeServers } = this.props;
		const { serversToDelete } = this.state;
		removeServers(serversToDelete);
	}

	addServer = () => {
		const { newServer } = this.props;
		newServer();
	}

	getTotalTime = () => {
		let { servers } = this.props;
		if(servers) {
			servers = servers.filter(server => server.RemoveDateTime);
		}

		const converTimes = (timeObject) => {
			if(timeObject.mins >= 60) {
				let hours = parseInt(timeObject.mins/60);
				timeObject.mins =  timeObject.mins - hours*60;
				timeObject.hours = timeObject.hours + hours;
			}
			
			if(timeObject.secs >= 60) {
				let mins = parseInt(timeObject.secs/60);
				timeObject.mins = timeObject.mins + mins;
				timeObject.secs = timeObject.secs - mins*60;
			}

			return timeObject;
		}

		const calcucalteTime = (servers, filedName = false) => {
			let times = servers.map(server => {
			let hours = parseInt(moment(filedName ? server[filedName] : new Date()).diff(moment(server.CreateDateTime), 'hours', true));
			let minutes = moment(filedName ? server[filedName] : new Date())
				.subtract(hours, 'hours')
				.diff(moment(server.CreateDateTime), 'minutes', true);
			minutes = parseInt(minutes);
			let seconds = moment(filedName ? server[filedName] : new Date())
				.subtract(hours, 'hours')
				.subtract(minutes, 'minutes')
				.diff(moment(server.CreateDateTime), 'seconds', true);
			seconds = parseInt(seconds);
				return {
					hours: hours,
					mins: minutes,
					secs: seconds
				};
			});
			let all = {
				hours: 0,
				mins: 0,
				secs: 0
			};		
			times.forEach(time => {
				all.hours += time.hours;
				all.mins += time.mins;
				all.secs += time.secs;
			});

			all = converTimes(all);
			
			return all;
		}
		let allTimes1, allTimes2;
		if(servers) {
			allTimes1 = calcucalteTime(servers, 'RemoveDateTime');
		};

		let _servers;
		if(this.props.servers) {
			_servers = this.props.servers.filter(server => !server.RemoveDateTime);
		}
		if(_servers) {
			allTimes2 = calcucalteTime(_servers,);
		}
		let total = converTimes({
			hours: allTimes1 ? allTimes1.hours: 0 + allTimes2 ? allTimes2.hours : 0,
			mins: allTimes1 ? allTimes1.mins : 0 + allTimes2 ? allTimes2.mins: 0,
			secs: allTimes1 ? allTimes1.secs : 0 + allTimes2 ? allTimes2.secs : 0,
		});

		return `${total.hours < 10 ? `0${total.hours}`: total.hours}:${total.mins < 10 ? `0${total.mins}` : total.mins}:${total.secs < 10 ? `0${total.secs}` : total.secs}`
	}

	componentDidMount() {
		setInterval(() => {
			this.forceUpdate();
		}, 1000);
	}

	render() {
		const { servers } = this.props;
		return (
			<React.Fragment>
				<div className="current-time"><span className="label-current">CurrentDateTime:</span><span>{moment().format('YYYY-MM-DD HH:mm:ss')}</span></div>
				<div className="total-time"><span className="label-use">TotalUsageTime: </span><span className="time">{this.getTotalTime()}</span></div>
				<div className="server-list">
					<div className="server header">
						<div className="server-id">VirtualServerId</div>
						<div className="server-create">CreateDateTime</div>
						<div className="server-remove">RemoveDateTime</div>
						<div className="server-select">SelectedForRemove</div>
					</div>
					{servers && servers.map(server => {
						return (
							<Server
								server={server}
								setForDelete={this.setForDelete}
								removeFromDelete={this.removeFromDelete}
							/>						
						);
					})}
					<div className="controls">
						<Button
							onClick={this.addServer}
							variant="contained"
							color="primary"
						>
							ADD
						</Button>
						<Button
							disabled={this.state.serversToDelete.length === 0}
							onClick={this.deleteServers}
							variant="contained"
							color="primary"
						>
							REMOVE
						</Button>
					</div>
				</div>
			</React.Fragment>
		);
	}
}