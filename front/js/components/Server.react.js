import React, {Component} from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import moment from 'moment';

export default class Server extends Component {
	constructor(props) {
		super(props);

		this.state = {
			checked: false,
		};
	}

	changeChecked = (id) => (e) => {
		e.preventDefault();
		e.stopPropagation();
		const { setForDelete, removeFromDelete } = this.props;
		if(this.state.checked) {
			removeFromDelete(id);
		} else {
			setForDelete(id);
		}
		this.setState((state) => {
			return {
				checked: !state.checked,
			}
		});
	}

	render() {
		const { server } = this.props;
		return (
			<div key={server.VirtualServerId} className="server">
				<div className="server-id">{server.VirtualServerId}</div>
				<div className="server-create">{moment(server.CreateDateTime).format('YYYY-MM-DD HH:mm:ss')}</div>
				<div className="server-remove">{server.RemoveDateTime ? moment(server.RemoveDateTime).format('YYYY-MM-DD HH:mm:ss') : ''}</div>
				<div className="server-select">
					<Checkbox disabled={!!server.RemoveDateTime} onClick={this.changeChecked(server.VirtualServerId)} checked={this.state.checked} />
				</div>
			</div>
		);
	}
}