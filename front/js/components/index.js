import React, {Component} 	from 'react';
import { bindActionCreators } from 'redux';
import * as helper 			from '../utils/helper';
import { fetchServers, deleteServers, addServer } 	from '../actions/servers';
import ServerList 			from './ServerList.react';
import { connect } from 'react-redux';

// styles
import './index.scss';
class MainApp extends Component {
	constructor(props) {
		super(props);

		this.state = {
			loading: false,
		};
	}

	render() {
		const { items } = this.props.servers;
		const { removeServers, newServer } = this.props;
		console.log('items', process.env);
		return (
			<main className="main-board">
				<ServerList
					servers 		= {items}
					removeServers 	= {removeServers}
					newServer 		= {newServer}
				/>
			</main>
		);
	}

	componentWillMount() {
		const { getServers } = this.props;
		getServers();
	}
}

const mapStateToProps = (state) => {
	return state;
}

const mapDispatchToProps = dispatch => bindActionCreators({
	getServers: () => fetchServers(),
	removeServers: (ids) => deleteServers(ids),
	newServer: (ids) => addServer(ids),
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainApp);

