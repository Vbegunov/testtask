import React 	from 'react';
import ReactDOM from 'react-dom';
import App 		from './components/index';
import { Provider } 		from 'react-redux'
import { createStore, applyMiddleware, combineReducers } 	from 'redux';
import thunkMiddleware 		from 'redux-thunk'
import reducers from './reducers/';
const store = createStore(	
	reducers,
	applyMiddleware(thunkMiddleware)
);

const appRoot = document.querySelector('#app-root');

if(appRoot) {
	ReactDOM.render(
		<Provider store={store}>
			<App/>	
		</Provider>
		,
		appRoot
	);
}