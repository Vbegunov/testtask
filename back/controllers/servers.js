import Servers 	from '../models/servers';
import bluebird from 'bluebird';
import redis 	from 'redis';


const DEFAULT_CACHE_TIME_AFTER_POST = process.env.POST_TIMEOUT;//5 mins
const DEFAULT_CACHE_TIME_AFTER_DELETE = process.env.DELETE_TIMEOUT;//3 mins

var client = bluebird.promisifyAll(redis.createClient(6379, '127.0.0.1'));

const filterServers = (arrayToFilter, mask) => {
	arrayToFilter = arrayToFilter.filter(server => {
		let index = mask.findIndex(item => {
			if(item.VirtualServerId === server.VirtualServerId) {
				return true;
			} else return false;
		});

		if(index > -1) {
			return false;
		} else return true;
	});

	return arrayToFilter;
}

const filterDeletedServers = (arrayToFilter, mask) => {
	arrayToFilter.map(server => {
		let index = mask.findIndex(item => {
			if(item.VirtualServerId === server.VirtualServerId) {
				return true;
			} else return false;
		})

		if(index > -1) {
			server.RemoveDateTime = '';
		}
		return server;
	});
	return arrayToFilter;
}

exports.list = async (req, res) => {
	// get cached servers from post
	let posted = await client.getAsync('_SERVERS_POST');
	posted = JSON.parse(posted);
	// get cached servers from delete
	let deleted = await client.getAsync('_SERVERS');
	deleted = JSON.parse(deleted);
	// get servers from a db
	let all = await Servers.find({ });

	if(deleted) {
		all = filterDeletedServers(all, deleted);
	}

	if(posted) {
		all = filterServers(all, posted);
	}

	res.json({servers: all});
};

exports.flush = (req, res) => {
	client.flushdb();
	res.json({ok: true});
}

// create server by id
exports.post = async (req, res) => {
	const server = {};
	server.CreateDateTime = new Date();
	let item;
	let forCache = await Servers.find({ });
	let result = await Servers.create(server);
	item = [result];
	let posted = await client.getAsync('_SERVERS_POST');
	if(posted) {
		posted = JSON.parse(posted);
		item = [...posted, ...res];
	}
	client.setex('_SERVERS_POST', DEFAULT_CACHE_TIME_AFTER_POST, JSON.stringify(item));

	res.json({server: result});
};

// delete server by id
exports.delete = async (req, res) => {
	const { ids } = req.body;
	const server = {};
	server.RemoveDateTime = new Date();
	
	let deleteSerevers = await Servers.find({ VirtualServerId: {
		$in: ids
	} });

	client.setex('_SERVERS', DEFAULT_CACHE_TIME_AFTER_DELETE, JSON.stringify(deleteSerevers));
	await Servers.updateMany(
		{ VirtualServerId: { $in: ids} },
		{ $set: { RemoveDateTime: new Date } }
	).exec();
	res.json({ids: ids});
}