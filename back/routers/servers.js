import express from 'express';
import Servers from '../controllers/servers';
const Router = express.Router();

// get all servers
Router.get('/', Servers.list);
// create new server 
Router.post('/', Servers.post);
Router.post('/flush', Servers.flush);
// delete server by id
Router.delete('/', Servers.delete);

module.exports = Router;