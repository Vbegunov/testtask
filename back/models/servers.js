import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const { Schema } = mongoose;

const serverSchema = new Schema(
	{
		VirtualServerId: {
			type: String,
			unique: true,
			required: true,
		},
		CreateDateTime: Date,
		RemoveDateTime: Date,
	},
  	{ collection: 'servers' },
);

autoIncrement.initialize(mongoose.connection);
serverSchema.plugin(autoIncrement.plugin, { model: 'Server', field: 'VirtualServerId', startAt: 1 });
serverSchema.index({ VirtualServerId: 1 });
module.exports = mongoose.model('Server', serverSchema);