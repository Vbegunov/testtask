import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
const dbConnection = mongoose.connect(process.env.DATABASE_URL);

dbConnection.then((db) => {
	console.log('Successfully connected to DB');
	return db;
  }).catch((err) => {
	if (err.message.code === 'ETIMEDOUT') {
		// retry connection;
		mongoose.connect(process.env.DATABASE_URL);
	} else {
		console.log('An error occured while connecting to DB');
	}
  });

module.exports = dbConnection;
