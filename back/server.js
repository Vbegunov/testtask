import express from 'express';
import cors from 'cors';
import 'dotenv/config';
import './utils/db';
import bodyParser from 'body-parser';

// routers
import ServersRoutes from './routers/servers';

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/api/v1/servers', ServersRoutes);

app.listen(3000, () => {
	console.log('Server is running on port 3000');
});